+++
title = "Willkommen zu Jabber"
+++

Willkommen zur Jabbergemeinschaft. Auf dieser Webseite helfen wird dir einen Server und einen Anwendung zu finden um am Jabbernetzwerk teilzunehmen und mit deinen Freunden zu chatten, ohne deine Privatspäre zu gefärden. Jabber ist dezentralisiert, ähnlich wie Email. Das bedeuted das du zu erst einen Server finden muss und von dort kann du dann mit jedem Anderen chatten.

Dies könnte erst einmal etwas verwirrend sein, aber eine Jabberadresse (JID) sieht genauso wie eine Emailadresse aus. Wenn du zum Beispiel einen Account `emma.goldman` auf dem Server jabber.fr hast, ist deine Adresse `emma.goldman@jabber.fr`. Chatrooms (auch MUCs genannt) haben ebenfalls eine ähnliche Adresse, so wie `chat@joinjabber.org`.

Also lass uns anfangen!

# Schritt 1: Registiere einen Account auf einem Server

Abhängig von deinen Anforderungen, können wir unterschiedliche Server empfehlen. Einfach auf den Anwendungsfall auswählen um mehr herauszufinden.

<div class="usecases" style="text-align: center">
<details>
<summary>
{{ usecase(usecase="personal") }}
</summary>
<p>Für dich persönlich ist es gut nach einem nicht-kommerziellen und nachhaltig betriebenen Serveranbieter zu schauen der wahrscheinlich auch noch in 20 Jahren noch im Betrieb sein wird. Wir empfehlen:</p>
{{ server(usecase="personal") }}
</details>
<details>
<summary>
{{ usecase(usecase="collective") }}
</summary>
<p>Für deine Kollektiv/Organisation, kannst du <a href="https://homebrewserver.club/category/instant-messaging.html">deinen eigenen Server aufsetzen</a>, zum Beispiel mit <a href="https://yunohost.org">Yunohost</a>. Wenn dies nicht machbar ist, empfehlen wir die folgenen professionellen Anbieter:</p>
</details>
<details>
<summary>
{{ usecase(usecase="pseudo") }}
</summary>
<p>Für politischen Aktivismus gibt es pseudonyme Jabberanbieter. Hier ein paar Beispiele:</p>
{{ server(usecase="pseudo") }}
</details>
</div>



# Schritt 2: Wähle einen Client

Jetzt wo du einen Server hast, kannst du einen Client herunterladen und einrichten. Im Folgenden einige von uns empfohlende Clients die mit Hinsicht auf die Bedürfnisse der Jabbergemeinschaft entwickelt werden. Einfach auf das Betriebssystem clicken um zur Dokumentation zu gelangen.

<div class="platforms" style="text-align: center">

{{ platform(platform="android") }}
{{ platform(platform="ios") }}


{{ platform(platform="gnulinux") }}
{{ platform(platform="windows") }}
{{ platform(platform="macos") }}
</div>

# Schritt 3: Alles klar!

Glückwunsch! Du hast jetzt einen Server und Client. Du kannst deinen Client öffnen und deine Jabberadresse eingeben um mit deinem Server zu verbinden und dann mit mit deinen Freunden zu chatten. Wenn du noch niemanden auf Jabber kennst, kannst du natürlich auch mit uns chatten: [chat@joinjabber.org](xmpp:chat@joinjabber.org?join)
